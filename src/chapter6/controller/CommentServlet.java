package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.service.CommentService;

/**
 * Servlet implementation class CommentServlet
 */
@WebServlet("/comment")
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String text = request.getParameter("text");
		int messageId = Integer.parseInt(request.getParameter("messageId"));
		User user = (User) request.getSession().getAttribute("loginUser");

		List<String> errorMessages = new ArrayList<String>();
		if(!isValid(text, errorMessages)) {
			request.getSession().setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}


		Comment comment = new Comment();

		comment.setText(text);
		comment.setMessage_id(messageId);
		comment.setUser_id(user.getId());

		new CommentService().insert(comment);

		response.sendRedirect("./");
	}

    private boolean isValid(String text, List<String> errorMessages) {

    	if(StringUtils.isBlank(text)) {
    		errorMessages.add("メッセージを入力してください");
    	} else if(140 < text.length()) {
    		errorMessages.add("140文字以下で入力してください");
    	}

    	if(errorMessages.size() != 0) {
    		return false;
    	}
    	return true;
    }


}
