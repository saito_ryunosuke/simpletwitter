package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.service.MessageService;

/**
 * Servlet implementation class DeleteMessgeServlet
 */
@WebServlet("/deleteMessage")
public class DeleteMessgeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int deleteId = Integer.parseInt(request.getParameter("deleteId"));

		new MessageService().delete(deleteId);
		response.sendRedirect("./");
	}

}
