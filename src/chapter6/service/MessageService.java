package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {
	public void insert(Message message) {

		Connection connection = null;
		try {

			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userId, String start, String end){
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();

			if(!StringUtils.isEmpty(start)) {
				start += " 00:00:00";
			} else {
				start = "2021/01/01 00:00:00";
			}

			if(!StringUtils.isEmpty(end)) {
				end += " 23:59:59";
			} else {
				Date currentDay = new Date();
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				String defaultDay = format.format(currentDay);
				end = defaultDay;
			}

			Integer id = null;
			if(!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}
			List<UserMessage> message = new UserMessageDao().select(connection, id,  LIMIT_NUM, start, end);
			commit(connection);

			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int deleteId) {
		Connection connection = null;
		try {
			connection = getConnection();

			new MessageDao().delete(connection, deleteId);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

	public Message select(int editId) {
		Connection connection = null;
		try {
			connection = getConnection();

			Message editMessage = new MessageDao().select(connection, editId);

			commit(connection);

			return editMessage;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

	public void update(int editId, String text) {
		Connection connection = null;
		try {
			connection = getConnection();

			new MessageDao().update(connection, editId, text);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}



}
