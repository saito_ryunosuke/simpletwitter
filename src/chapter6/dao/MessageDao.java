package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("    user_id, ");
			sql.append("    text, ");
			sql.append("    created_date, ");
			sql.append("    updated_date ");
			sql.append(") VALUES ( ");
			sql.append("    ?, "); // user_id
			sql.append("    ?, "); // text
			sql.append("    CURRENT_TIMESTAMP, "); // created_date
			sql.append("    CURRENT_TIMESTAMP "); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getUser_id());
			ps.setString(2, message.getText());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void  delete(Connection connection, int deleteId) {

		PreparedStatement ps = null;
		try {

			String sql = "DELETE FROM messages WHERE id = ?";

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, deleteId);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public Message select(Connection connection, int editId) {

		PreparedStatement ps = null;
		try {

			String sql = "SELECT * FROM messages WHERE id = ?";
			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, editId);

			ResultSet rs = ps.executeQuery();
			List<Message> editMessages = toMessages(rs);

			if(editMessages.isEmpty()) {
				return null;
			} else {
				return editMessages.get(0);
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Message> toMessages(ResultSet rs) throws SQLException {

		List<Message> messages = new ArrayList<Message>();
		try {
			while (rs.next()) {
				Message message = new Message();
				message.setId(rs.getInt("id"));
				message.setText(rs.getString("text"));
				message.setUser_id(rs.getInt("user_id"));
				message.setCreatedDate(rs.getTimestamp("created_date"));
				message.setUpdatedDate(rs.getTimestamp("updated_date"));

				messages.add(message);
			}
			return messages;
		} finally {
			close(rs);
		}
	}

	public void update(Connection connection, int editId, String text) {

	    PreparedStatement ps = null;
	    try {
	        StringBuilder sql = new StringBuilder();
	        sql.append("UPDATE messages SET ");
	        sql.append("    text = ?, ");
	        sql.append("    updated_date = CURRENT_TIMESTAMP ");
	        sql.append("WHERE id = ?");

	        ps = connection.prepareStatement(sql.toString());

	        ps.setString(1, text);
	        ps.setInt(2, editId);



	        int count = ps.executeUpdate();
	        if (count == 0) {
	            throw new NoRowsUpdatedRuntimeException();
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}




}
