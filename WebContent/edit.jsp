<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./CSS/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/vender/jQueryMain.js"></script>
<script type="text/javascript" src="./js/vender/jQuery.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>つぶやき編集画面</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>

		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<div class="account">
					@
					<c:out value="${loginUser.account}" />
				</div>
				<div class="description">
					<c:out value="${loginUser.description}" />
				</div>
			</div>
		</c:if>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="form-area">
			<form action="edit" method="post">
				<input type="hidden" value="${editMessage.id}" name="editId">
				<textarea name="text" cols="100"  rows="5"  class="tweet-box" ><c:out value="${editMessage.text}"></c:out></textarea>
				<br /> <input class="update_button" type="submit" value="更新">（140文字まで）<br />
				<a href="./">戻る</a>
			</form>
		</div>

		<div class="copyright">Copyright(c)RyunosukeSaito</div>

	</div>
</body>
</html>