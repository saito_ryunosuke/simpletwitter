$(function() {
	$('.delete_button').on('click', function() {
		if(!confirm('本当に削除しますか？')){
			return false;
		} else {
			return true;
		}
	});
});

$(function() {
	$('.update_button').on('click', function() {
		if(!confirm('更新してもよろしいですか？')){
			return false;
		} else {
			return true;
		}
	});
});

$(function() {
	$('.logout').on('click', function() {
		if(!confirm('ログアウトしますか？')){
			return false;
		} else {
			return true;
		}
	});
});


$(function() {
	$('.reply').click(function(){
		$(this).children().show();
		$(this).children('.reply-button').hide();
	});
});