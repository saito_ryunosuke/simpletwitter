<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<link href="./CSS/style.css" rel="stylesheet" type="text/css">
<meta charset="UTF-8">
<title>ログイン</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>


		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="login" method="post"><br />
			<label for="accountOrEmail">アカウント名かメールアドレス</label><br />
			<input name="accountOrEmail" id="accountOrEmail" /> <br />

			<label for="password">パスワード</label><br />
			<input name="password" type="password"id="password" /> <br />

			<input type="submit" value="ログイン" /> <br />
			<a href="./">戻る</a>
		</form>

		<div class="copyright"> Copyright(c)RyunosukeSaito</div>
	</div>

</body>
</html>